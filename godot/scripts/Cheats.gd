extends Node

var boss: Node2D
var loot: Node
var item_to_spam: PackedScene

# Called when the node enters the scene tree for the first time.
func _ready():
	self.boss = get_node("/root/Base/Boss")
	self.loot = get_node("/root/Base/Loot")
	self.item_to_spam = preload("res://scenes/items/granny.tscn")

#func _input(event):
#	if event.is_action_released("ui_cheat_items"):
#		for i in range(10):
#			var spawned_item: Node2D = self.item_to_spam.instance()
#			spawned_item.position = $ItemSpawnPoint.position + Vector2(randi() % 100 - 50, randi() % 100 - 50)
#			spawned_item.activate()
#			self.loot.add_child(spawned_item)