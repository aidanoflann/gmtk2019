extends Timer

var has_played: bool = false
var has_triggered_fadeout: bool = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if self.has_played and self.is_stopped():
		get_tree().change_scene("res://scenes/EndGame.tscn")
		return
	
	if not self.has_triggered_fadeout and self.has_played and self.time_left < 1:
		$"/root/Base/UI/ScreenFader/AnimationPlayer".play_backwards("fade_to")
		self.has_triggered_fadeout = true
	
	if not self.has_played and not self.time_left < self.wait_time:
		self.has_played = true

func play_us_out():
	"""
	Start the timer, at the end of which we should transition to the EndGame scene.
	"""
	self.start()