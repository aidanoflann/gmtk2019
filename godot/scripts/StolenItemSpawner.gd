extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var stolen_items: Array = []
var end_game_text: RichTextLabel 
var text_template: String

# Called when the node enters the scene tree for the first time.
func _ready():
	self.end_game_text = get_node("/root/Base/CanvasHolder/UI/EndGameText")
	self.text_template = self.end_game_text.bbcode_text
	randomize()
	
	var y_offset = -10
	var x_options = [175, 200, 225, 250, 275, 300]
	for item_resource_path in globals.item_resource_paths:
		if item_resource_path == "":
			continue
		var stolen_item: Node2D = load(item_resource_path).instance()
		self.add_child(stolen_item)
		stolen_item.position = Vector2(x_options[randi()%x_options.size()], y_offset)
		stolen_item.activate()
		stolen_item.gravity_scale = 0
		stolen_item.base_scene_name = item_resource_path
		stolen_item.add_torque((randi()%10 + 0.5 - 5) * 100)
		y_offset -= 50
		self.stolen_items.append(stolen_item)
		self.add_child(stolen_item)
		
	$"/root/Base/EndGameTimer".time_to_go = int(round(len(self.stolen_items) * 0.6))

func spawn_items():
	for item in self.stolen_items:
		item.gravity_scale = 8

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var total_money: int = 0
	for stolen_item in self.stolen_items:
		if stolen_item.position.y > 50:
			total_money += stolen_item.get("value")
	
	self.end_game_text.bbcode_text = self.text_template.replace("0", str(total_money))
