extends RigidBody2D

var speed: float = 200  #max speed
var min_speed: float = 100
var running: bool = false
var stopping_point: Vector2
var plateau_point: Vector2
var brake_point: Vector2
var accel = 150
var decel = 100
		
func run_away():
	self.running = true
	self.stopping_point = get_node("/root/Base/RobberStoppingpoint").position
	self.plateau_point = get_node("/root/Base/RobberPlateau").position
	self.brake_point = get_node("/root/Base/RobberDecelerate").position
	$AnimationTree.get("parameters/playback").travel("move")
	
func _process(delta):
	if self.running:
		if self.position.x > self.stopping_point.x:
			self.linear_velocity = Vector2(0, 0)
#			$AnimationTree.get("parameters/playback").travel("idle")
			self.running = false
			
			# cut to next scene
			globals.item_resource_paths = []
			for loot in $StolenLoot.stolen_loot:
				if not is_instance_valid(loot) or loot == null:
					continue
				var base_scene_name: String = loot.get("base_scene_name")
				if base_scene_name != null:
					globals.item_resource_paths.append(base_scene_name)
			$"/root/Base/ExitTimer".play_us_out()


func get_x_vel(delta) -> float:
	var current_x_vel: float = self.get_linear_velocity().x
	var x_vel: float
	if not self.running or self.position.x > self.stopping_point.x:
		x_vel = 0.0
		return x_vel
	elif self.position.x > self.brake_point.x:
		x_vel = current_x_vel - self.decel * delta
	elif self.position.x > self.plateau_point.x:
		x_vel = current_x_vel
	else:
		x_vel = current_x_vel + self.accel * delta
		
	return max(self.min_speed, min(x_vel, self.speed))

func _integrate_forces(state):
	"""
	Freeze torque and y movement. Freeze x movement if not running
	"""
	var x_vel: float = self.get_x_vel(state.step)
#	if self.running:
#		x_vel = self.speed
#	else:
#		x_vel = 0
	state.set_linear_velocity (Vector2 (x_vel, 0))
	state.set_angular_velocity(0)
	