extends Node2D

var item_directory: Array = []
var spawn_cooldown: float = 4  # seconds
var time_since_cooldown: float = 0
var loot_container: Node
var hands: Array  # of Node2Ds
var held_items: Array  # of RigidBodys
var game_state: Node
var previous_animation_node: String
var audio_player: Node
var rob_timer: Timer

func _ready():
	randomize()
	self.item_directory = [
		preload("res://scenes/items/ashes.tscn"),
		preload("res://scenes/items/baguette.tscn"),
		preload("res://scenes/items/banana.tscn"),
		preload("res://scenes/items/banjo.tscn"),
		preload("res://scenes/items/cactus.tscn"),
		preload("res://scenes/items/cash.tscn"),
		preload("res://scenes/items/chest.tscn"),
		preload("res://scenes/items/clock.tscn"),
		preload("res://scenes/items/cookie.tscn"),
		preload("res://scenes/items/ducky.tscn"),
		preload("res://scenes/items/fork.tscn"),
		preload("res://scenes/items/gameboy.tscn"),
		preload("res://scenes/items/gemblue.tscn"),
		preload("res://scenes/items/gemgreen.tscn"),
		preload("res://scenes/items/gemred.tscn"),
		preload("res://scenes/items/granny.tscn"),
		preload("res://scenes/items/guitar.tscn"),
		preload("res://scenes/items/ingot.tscn"),
		preload("res://scenes/items/microwave.tscn"),
		preload("res://scenes/items/mirror.tscn"),
		preload("res://scenes/items/mocha.tscn"),
		preload("res://scenes/items/oldphone.tscn"),
		preload("res://scenes/items/peppermill.tscn"),
		preload("res://scenes/items/plant.tscn"),
		preload("res://scenes/items/potion.tscn"),
		preload("res://scenes/items/pug.tscn"),
		preload("res://scenes/items/rubik.tscn"),
		preload("res://scenes/items/safe.tscn"),
		preload("res://scenes/items/skateboard.tscn"),
		preload("res://scenes/items/suitcase.tscn"),
		preload("res://scenes/items/sword.tscn"),
		preload("res://scenes/items/teddy.tscn"),
		preload("res://scenes/items/toilet.tscn"),
		preload("res://scenes/items/tv.tscn"),
		preload("res://scenes/items/umbrella.tscn"),
		preload("res://scenes/items/washingmachine.tscn"),
		preload("res://scenes/items/wine.tscn"),
	]
	self.loot_container = get_node("/root/Base/Loot")
	self.audio_player = get_node("/root/AudioPlayer")
	self.hands = get_node("Hands").get_children()
	self.game_state = get_node("/root/Base/GameState")
	self.rob_timer = get_node("/root/Base/RobTimer")
			
func _process(delta):
	# clear other items if one was taken
	for held_item in self.held_items:
		# need some extra handling here to make scene reloading work
		if held_item == null:
			continue
		if not is_instance_valid(held_item):
			continue
		var held_by_boss: bool = held_item.get("held_by_boss")
		if held_by_boss == true:
			continue
			
		if self.rob_timer.is_stopped():
			self.rob_timer.start()
			
		self.clear_items()
		self.time_since_cooldown = 0
		if game_state.is_robbing():
			self.rob_more_stuff()
		break
	
	# DISABLED
#	self.time_since_cooldown += delta
#	if self.time_since_cooldown > self.spawn_cooldown:
#		self.clear_items()
#		self.time_since_cooldown -= self.spawn_cooldown
#		if game_state.is_robbing():
#			self.rob_more_stuff()
	
	var current_animation_node: String = $AnimationTree.get("parameters/playback").get_current_node() 
	if current_animation_node == "idle" and self.previous_animation_node == "rob":
		self.spawn_items()
	
	self.previous_animation_node = current_animation_node

func clear_items():
	while self.held_items.size() > 0:
		var held_item: RigidBody2D = self.held_items.pop_back()
		if held_item != null and is_instance_valid(held_item) and held_item.held_by_boss:
			held_item.queue_free()
		
func rob_more_stuff():
	# first despawn any held items
	self.clear_items()
	$AnimationTree.get("parameters/playback").travel("rob")
	
func spawn_items():
	# then spawn new ones
	var already_chosen_items: Array = [null]
	for hand in self.hands:
		var chosen_item: PackedScene = null
		while (chosen_item in already_chosen_items):
			chosen_item = self.item_directory[randi() % self.item_directory.size()]
		var spawned_item: Node2D = chosen_item.instance()
		spawned_item.position = hand.position
		spawned_item.base_scene_name = chosen_item.resource_path
		already_chosen_items.append(chosen_item)
		
		self.loot_container.add_child(spawned_item)
		self.held_items.append(spawned_item)

func say_hay():
	var heys = ["hey1", "hey2", "hey3"]
	self.audio_player.play(heys[randi() % heys.size()])
