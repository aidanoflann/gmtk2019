extends Timer

# Declare member variables here. Examples:
var triggered_start: bool = false

# Called when the node enters the scene tree for the first time.
func start(f:float=0.0):
	.start()
	$"/root/Base/CanvasLayer/ScreenFader/AnimationPlayer".play_backwards("fade_to")
	

func _process(delta):
	if self.is_stopped():
		return
	
	if not self.triggered_start and self.wait_time - self.time_left > 2:
		get_tree().change_scene("res://scenes/Base.tscn")
	
