extends KinematicBody2D

var velocity_direction: Vector2
var velocity_magnitude = 300
var drag = 20
var moving = false
var state_machine: AnimationNodeStateMachinePlayback
var audio_player: AudioPlayer

func _ready():
	self.state_machine = $AnimationTree.get("parameters/playback")
	self.audio_player = get_node("/root/Base/AudioPlayer")

func _input(event: InputEvent):
	if event.is_action_pressed("ui_right"):
		self.velocity_direction = Vector2(1, 0)
		self.audio_player.play("test")
		self.moving = true
	elif event.is_action_pressed("ui_left"):
		self.velocity_direction = Vector2(-1, 0)
		self.moving = true
	elif event.is_action_pressed("ui_up"):
		self.velocity_direction = Vector2(0, -1)
		self.moving = true
	elif event.is_action_pressed("ui_down"):
		self.velocity_direction = Vector2(0, 1)
		self.moving = true
	elif (
		event.is_action_released("ui_right") or
		event.is_action_released("ui_left") or
		event.is_action_released("ui_up") or
		event.is_action_released("ui_down")
		):
		self.moving = false
	if self.moving:
		self.state_machine.travel("moving")
	else:
		self.state_machine.travel("idle")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	self.move_and_collide(delta * self.velocity_direction * self.velocity_magnitude)
	if not self.moving:
		self.velocity_direction *= max((1 - self.drag * delta), 0)