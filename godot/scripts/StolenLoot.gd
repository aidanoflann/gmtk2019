extends Area2D

var stolen_loot: Array = []
	
func on_object_entered(object: PhysicsBody2D):
	self.stolen_loot.append(object)
	
func on_object_exited(object: PhysicsBody2D):
	self.stolen_loot.erase(object)
