extends KinematicBody2D

var velocity: Vector2
var velocity_magnitude = 30000
var drag = 20
var moving = false
var gravity = -9.8 * 1000

#func _physics_process(delta):
#	prints(get_colliding_bodies())

func _input(event: InputEvent):
	var move_dir: int
	if event.is_action_pressed("ui_right"):
		move_dir = +1
		self.moving = true
	elif event.is_action_pressed("ui_left"):
		move_dir = -1
		self.moving = true
	elif event.is_action_pressed("ui_up"):
		self.velocity.y = -10000
	elif (
		event.is_action_released("ui_up") or
		event.is_action_released("ui_down")
		):
		move_dir = 0
		self.moving = false
	self.velocity.x = move_dir * self.velocity_magnitude

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if self.is_on_floor():
		self.velocity.y = 0
	else:
		self.velocity.y -= delta * self.gravity
	
	self.move_and_slide(delta * self.velocity)