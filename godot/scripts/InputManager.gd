extends Node2D

var mouseover_item: RigidBody2D
var attached_item: RigidBody2D

func _input(event):
	if(event.is_action_pressed("ui_left_click")):
		if self.mouseover_item != null and is_instance_valid(self.mouseover_item) and self.mouseover_item.is_class("RigidBody2D"):
			self.attached_item = self.mouseover_item
			self.attached_item.anchor_point = self.attached_item.position - get_global_mouse_position()
		$"/root/Base/UI/Cursor".set_grabbing()
	elif(event.is_action_released("ui_left_click")):
		if self.attached_item != null:
			self.attached_item.moving_to_mouse = false
		self.attached_item = null
		$"/root/Base/UI/Cursor".set_idle()
	elif event is InputEventMouseMotion:
		if self.attached_item != null:
			self.attached_item.move_to_mouse()