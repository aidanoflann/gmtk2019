extends Node

func _input(event):
	if not OS.has_feature("debug"):
		return
		
	if event.is_action("ui_restart"):
		get_tree().reload_current_scene()
		if self.name == "DebuggerEndGame":
			get_tree().change_scene("res://scenes/Base.tscn")
