extends Node

# Declare member variables here. Examples:
var samples: Dictionary = {}
export var sfx_enabled: bool = true
export var music_enabled: bool = true

# Called when the node enters the scene tree for the first time.
func _ready():
	for child in self.get_children():
		self.samples[child.name] = child

func play(sample_name: String) -> void:
	if not OS.has_feature("debug") or self.sfx_enabled:
		var sample: AudioStreamPlayer = self.samples.get(sample_name)
		if sample == null:
			self.samples.get("ashes").play()
		else:
			sample.play()
			
func start_theme_loop():
	if not OS.has_feature("debug") or self.music_enabled:
		self.play("thieves_loop")