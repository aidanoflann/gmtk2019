extends Timer

var triggered_sack: bool = false
var triggered_items: bool = false
var triggered_go: bool = false
var triggered_fade_in: bool = false

var time_to_go: int = -1

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not self.triggered_fade_in:
		self.triggered_fade_in = true
		$"/root/Base/CanvasHolder/UI/ScreenFader/AnimationPlayer".play("fade_to")
	
	if self.wait_time - self.time_left > 1 and not self.triggered_sack:
		self.triggered_sack = true
		$"/root/Base/results_sack/AnimationPlayer".play("shake")

	if self.wait_time - self.time_left > 1.2 and not self.triggered_items:
		self.triggered_items = true
		$"/root/Base/StolenItemSpawner".spawn_items()
#
	if self.time_to_go != -1:
		if self.wait_time - self.time_left > self.time_to_go and not self.triggered_go:
			self.triggered_go = true
			$"/root/Base/CanvasHolder/UI/RetryText".visible = true


func _input(event):
	if(event.is_action_pressed("ui_left_click")):
		$"/root/Base/CanvasHolder/UI/Cursor".set_grabbing()
	elif(event.is_action_released("ui_left_click")):
		$"/root/Base/CanvasHolder/UI/Cursor".set_idle()
		
	if self.triggered_go and event.is_action_released("ui_left_click"):
		get_tree().change_scene("res://scenes/Base.tscn")