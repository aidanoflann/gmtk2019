extends Timer

var game_state: Node
var stolen_loot: Area2D
var timer_ran: bool = false
var score_display: RichTextLabel

# Called when the node enters the scene tree for the first time.
func _ready():
	self.game_state = get_node("/root/Base/GameState")
	self.stolen_loot = get_node("/root/Base/Robbers/StolenLoot")
	self.score_display = get_node("/root/Base/UI/ScoreDisplay")

func _process(delta):
	if not self.game_state.is_running():
		return

func on_timer_finish():
	var score: int = 0
#	globals.stolen_items = self.stolen_loot.stolen_loot + []
	
	for loot in self.stolen_loot.stolen_loot:
		if loot == null:
			continue
		var value: int = loot.get("value")
		if value:
			score += value
	self.timer_ran = true
	
#	self.score_display.text = "You stole: $%d" % score
