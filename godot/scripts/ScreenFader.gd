extends Sprite

# Declare member variables here. Examples:
export var start_invisible: bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	if start_invisible:
		self.modulate.a = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
