extends Node

enum GameState {
	ROBBING
	RUNNING
	}

var state: int = GameState.ROBBING
var robbers: RigidBody2D
var disappearing_wall: TileMap

func _ready():
	self.robbers = get_node("/root/Base/Robbers")
	self.disappearing_wall = get_node("/root/Base/Disappearingwall")

func progress_to_running():
	self.state = GameState.RUNNING
	self.disappearing_wall.position = Vector2(1000, 1000)  # fuck off, wall
	self.robbers.run_away()
	
func is_robbing():
	return self.state == GameState.ROBBING

func is_running():
	return self.state == GameState.RUNNING