extends Timer

var progress_bar: TextureProgress
var progress_bar_animation_player: AnimationPlayer
var game_state: Node
var cop_car: Sprite
var car_start: Vector2
var car_stop: Vector2
var timer_ran: bool = false

export var timer_override = 5

# Called when the node enters the scene tree for the first time.
func _ready():
	self.progress_bar = get_node("/root/Base/UI/ProgressBar")
	self.progress_bar_animation_player = get_node("/root/Base/UI/ProgressBar/AnimationPlayer")
	self.game_state = get_node("/root/Base/GameState")
	self.cop_car = $"/root/Base/UI/ProgressBar/CopCar"
	self.car_start = $"/root/Base/UI/ProgressBar/StartPoint".position
	self.car_stop = $"/root/Base/UI/ProgressBar/StopPoint".position
	self.cop_car.position.x = round(self.car_start.x)
	self.cop_car.position.y = round(self.car_start.y)
	
	if OS.has_feature("debug"):
		self.wait_time = self.timer_override
		

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not self.is_stopped():
		self.timer_ran = true
	
	if not self.timer_ran:
		return
	
	var progress: float = (1 - float(self.time_left)/float(self.wait_time))
	
	if progress > 0.8:
		progress_bar_animation_player.play("flash")
	
	self.progress_bar.value = progress * 100
	if self.time_left <= 0 and self.game_state.is_robbing():
		self.game_state.progress_to_running()
		$"/root/DefaultCamera".follow($"/root/Base/Robbers")
	var lerp_point: Vector2 = lerp(self.car_start, self.car_stop, progress)
	self.cop_car.position.x = round(lerp_point.x)
	self.cop_car.position.y = round(lerp_point.y)
