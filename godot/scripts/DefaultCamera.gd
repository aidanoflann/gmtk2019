extends Camera2D

# Declare member variables here. Examples:
var target_node: Node2D = null
var target_offset: Vector2
var starting_position: Vector2

func _ready():
	self.starting_position = self.position + Vector2(0 ,0)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not is_instance_valid(self.target_node):
		self.target_node = null
	
	if self.target_node != null:
		self.smoothing_enabled = true
		self.position = target_node.position + self.target_offset
	else:
		self.smoothing_enabled = false
		self.position = self.starting_position

func follow(node: Node2D) -> void:
	self.target_node = node
	self.target_offset = self.position - node.position