extends RigidBody2D

# Declare member variables here. Examples:
var attached: bool = false
var input_manager: Node2D
var moving_to_mouse = false
var held_by_boss = true
var collision_polygon: CollisionPolygon2D
var active: bool = false
var max_speed: float = 700
var base_scene_name: String
var time_of_last_audio: int
var anchor_point: Vector2

export var audio_cooldown: int = 5  # s
export var value: int = 1

func _ready():
	self.input_manager = get_node("/root/Base/InputManager")
	self.collision_polygon = get_node("CollisionPolygon2D")
#	for child in self.get_children():
#		child.scale *= 0.5

func _process(delta):
	if self.position.y > 400:
		self.queue_free()

func move_to_mouse():
	self.moving_to_mouse = true
	self.held_by_boss = false
	self.activate()
	
func _physics_process(delta):
	if self.moving_to_mouse:
		var to_mouse: Vector2 = get_global_mouse_position() - self.position# + self.anchor_point
		self.linear_velocity = to_mouse * 10
		if self.linear_velocity.length() > max_speed:
			self.linear_velocity = self.linear_velocity.normalized() * max_speed
			
		# apply torque based on where

func on_mouse_entered():
	if self.input_manager != null:
		self.input_manager.mouseover_item = self

func on_mouse_exited():
	if self.input_manager != null:
		self.input_manager.mouseover_item = null
	
func activate():
	if not self.active:
		self.gravity_scale = 24
#		for child in self.get_children():
#			child.scale *= 2
		self.active = true
		
func on_body_entered(body: Node):
	var scene_name_clipped: String = self.base_scene_name.split("/")[-1].split(".")[0]
	var audio_allowed = false
	
	if self.time_of_last_audio + self.audio_cooldown < OS.get_unix_time():
		audio_allowed = true
		self.time_of_last_audio = OS.get_unix_time()
	
	if audio_allowed:
		get_node("/root/AudioPlayer").play(scene_name_clipped)