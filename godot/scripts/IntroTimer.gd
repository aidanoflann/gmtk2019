extends Timer

var fade_animation_player: AnimationPlayer
var boss: Node2D
var audio_triggered: bool = false
var fade_triggered: bool = false
var fade_animation_enabled: bool = false

func _ready():
	self.fade_animation_player = $"/root/Base/UI/ScreenFader/AnimationPlayer"
	if not OS.has_feature("debug") or self.fade_animation_enabled:
		$"/root/Base/UI/ScreenFader".visible = true
	self.boss = $"/root/Base/Boss"

func _process(delta):
	if not self.audio_triggered and self.time_left < 2.5:
		self.audio_triggered = true
		$"/root/AudioPlayer".play("intro")
		
	if not self.fade_triggered and self.time_left < 0.1:
		self.fade_triggered = true
		self.fade_animation_player.play("fade_to")
		self.boss.rob_more_stuff()
	
