extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	self.z_index = 20

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var global_mouse_pos: Vector2 = get_global_mouse_position()
	self.position.x = round(global_mouse_pos.x)
	self.position.y = round(global_mouse_pos.y)

func set_grabbing():
	$Idle.visible = false
	$Grabbing.visible = true

func set_idle():
	$Idle.visible = true
	$Grabbing.visible = false
