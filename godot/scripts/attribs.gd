extends RichTextLabel

# Declare member variables here. Examples:
var the_boys: Array = [
	"@_t3nshi - pixel art",
	"@sakana_cookies - music & SFX",
	"@DasGereon - design & SFX",
	"@itsjustaidan - code"
]

var placeholders: Array = [
	"<1>",
	"<2>",
	"<3>",
	"<4>"
]

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	self.the_boys.shuffle()
	var i: int = 0
	for boy in self.the_boys:
		self.bbcode_text = self.bbcode_text.replace(self.placeholders[i], boy)
		i += 1

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
